package com.yescare.springconfig.exception;

public class LoginFailedException extends RuntimeException {

    public LoginFailedException() {
        super(ErrorCode.Login_FAILED.getMessage());
    }

    private LoginFailedException(String message) {
        super(message);
    }
}
