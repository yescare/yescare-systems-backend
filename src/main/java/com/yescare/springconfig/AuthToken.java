package com.yescare.springconfig;

public interface AuthToken<T> {
    boolean validate();
    T getData();
}
