package com.yescare.springconfig;

import com.yescare.member.domain.Member;

import java.util.Optional;

public interface LoginUseCase {
    Optional<Member> login(String id, String password);
    AuthToken createAuthToken(Member member);
}
