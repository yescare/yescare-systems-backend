package com.yescare.springconfig.service;

import com.yescare.member.domain.Member;
import com.yescare.member.domain.MemberRepository;
import com.yescare.member.domain.Role;
import com.yescare.springconfig.AuthToken;
import com.yescare.springconfig.JwtAuthTokenProvider;
import com.yescare.springconfig.LoginUseCase;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Slf4j
@Service

public class LoginService {

    private final AuthenticationManagerBuilder authenticationManagerBuilder;
    private final JwtAuthTokenProvider jwtAuthTokenProvider;
    private final static long LOGIN_RETENTION_MINUTES = 30;
    private final PasswordEncoder passwordEncoder;
    private final MemberRepository memberRepository;

    @Autowired
    public LoginService(AuthenticationManagerBuilder authenticationManagerBuilder,
                        JwtAuthTokenProvider jwtAuthTokenProvider,
                        PasswordEncoder passwordEncoder,
                        MemberRepository memberRepository) {
        this.authenticationManagerBuilder = authenticationManagerBuilder;
        this.jwtAuthTokenProvider = jwtAuthTokenProvider;
        this.passwordEncoder = passwordEncoder;
        this.memberRepository = memberRepository;
    }

    public Optional<Member> login(String loginId, String password) {

        Member member = memberRepository.selectByLoginId(loginId)
                .orElseThrow(() ->  new UsernameNotFoundException("사용자를 찾을 수 없습니다."));

        boolean isCredential = passwordEncoder.matches(password, member.getPassword());
        log.info("check password=" + isCredential + ", password=" + password + ", old=" + member.getPassword());

        Set<GrantedAuthority> authoritySet = new HashSet<>();
        log.info("member.getRole()=" + member.getRole());

        authoritySet.add(new SimpleGrantedAuthority(member.getRole().getCode()));

        UsernamePasswordAuthenticationToken authenticationToken =
                new UsernamePasswordAuthenticationToken(loginId, password, authoritySet);

        return Optional.ofNullable(member);
    }

}
