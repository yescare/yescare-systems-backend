package com.yescare.springconfig;

import com.yescare.member.domain.Member;
import com.yescare.member.domain.MemberRepository;
import com.yescare.member.domain.Role;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.*;

@Slf4j
@RequiredArgsConstructor
@Service
public class CustomUserDetailService implements UserDetailsService {

    private final MemberRepository memberRepository;

    @Override
    public UserDetails loadUserByUsername(String loginId) throws UsernameNotFoundException {
        log.info("find user===>");
        Member member = memberRepository.selectByLoginId(loginId)
                .orElseThrow(() ->  new UsernameNotFoundException("사용자를 찾을 수 없습니다."));

        Set<GrantedAuthority> authoritySet = new HashSet<>();
        log.info("member.getRole()=" + member.getRole());

        authoritySet.add(new SimpleGrantedAuthority(member.getRole().getCode()));

        return new User(member.getId(), member.getPassword(), authoritySet);
    }

    private User createSpringSecurityUser(Member member) {
        List<GrantedAuthority> grantedAuthorities = Collections.singletonList(new SimpleGrantedAuthority("USER"));
        return new User(member.getId(), member.getPassword(),grantedAuthorities);
    }
}
