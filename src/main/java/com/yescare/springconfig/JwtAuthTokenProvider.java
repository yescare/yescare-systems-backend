package com.yescare.springconfig;

import com.yescare.member.domain.Member;
import com.yescare.member.domain.Role;
import io.jsonwebtoken.*;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import java.security.Key;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Component
@RequiredArgsConstructor
public class JwtAuthTokenProvider {

    //private String secretKey = "test";
    @Value("${jwt.base64.secret}")
    private String secretKey;

    private final static long LOGIN_RETENTION_MINUTES = 30;

    private final static long LOGOUT_MINUTES = 1;

    private final CustomUserDetailService userDetailsService;

    // 객체 초기화, secretKey를 Base64로 인코딩한다.
    @PostConstruct
    protected void init() {
        secretKey = Base64.getEncoder().encodeToString(secretKey.getBytes());
    }

    // JWT 토큰 생성
    public String createToken(Member member) {
        String loginId = member.getLoginId();
        Role role = member.getRole();

        Claims claims = Jwts.claims().setSubject(loginId); // JWT payload 에 저장되는 정보단위
        claims.put("role", role); // 정보는 key / value 쌍으로 저장된다.
        Date expireDate = Date.from(LocalDateTime.now().plusMinutes(LOGIN_RETENTION_MINUTES)
                            .atZone(ZoneId.systemDefault()).toInstant());

        return Jwts.builder()
                .setClaims(claims) // 정보 저장
                .setIssuedAt(new Date()) // 토큰 발행 시간 정보
                .setExpiration(expireDate) // set Expire Time
                .signWith(SignatureAlgorithm.HS256, secretKey)  // 사용할 암호화 알고리즘과 signature 에 들어갈 secret key
                .compact();
    }

    // JWT 토큰 생성
    public String invalidateToken(String token) {

        String userId = getTokenBodyByUserInfo(token);

        Claims claims = Jwts.claims().setSubject(userId); // JWT payload 에 저장되는 정보단위

        Date expireDate = Date.from(LocalDateTime.now().minusMinutes(LOGOUT_MINUTES)
                .atZone(ZoneId.systemDefault()).toInstant());

        log.info("expireDate=" + expireDate);

        return Jwts.builder()
                .setClaims(claims) // 정보 저장
                .setIssuedAt(new Date()) // 토큰 발행 시간 정보
                .setExpiration(expireDate) // set Expire Time
                .signWith(SignatureAlgorithm.HS256, secretKey)  // 사용할 암호화 알고리즘과 signature 에 들어갈 secret key
                .compact();
    }

    // JWT 토큰에서 인증 정보 조회
    public Authentication getAuthentication(String token) {
        UserDetails userDetails = userDetailsService.loadUserByUsername(this.getTokenBodyByUserInfo(token));
        // TODO 비밀번호 재인증 로직 추가 영역
        return new UsernamePasswordAuthenticationToken(userDetails, userDetails.getPassword(), userDetails.getAuthorities());
    }

    // 토큰에서 회원 정보 추출
    public String getTokenBodyByUserInfo(String token) {
        String getBodyInfo = Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token).getBody().getSubject();
        log.info(":: getBodyInfo="+getBodyInfo);
        return Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token).getBody().getSubject();
    }

    // Request Header 에서 token 값 추출 : "X-AUTH-TOKEN"
    public String resolveToken(HttpServletRequest request) {
        return request.getHeader("X-AUTH-TOKEN");
    }

    // 토큰의 유효성 + 만료일자 확인
    public boolean validate(String jwtToken) {
        try {
            Jws<Claims> claims = Jwts.parser().setSigningKey(secretKey).parseClaimsJws(jwtToken);

            log.info(":: validateToken=" + claims.getBody().getExpiration().before(new Date()));
            log.info(":: getData(jwtToken)=" + getData(jwtToken));

            return !claims.getBody().getExpiration().before(new Date());
        } catch (Exception e) {
            return false;
        }
    }

    public Claims getData(String jwtToken) {

        try {
            return Jwts.parser().setSigningKey(secretKey).parseClaimsJws(jwtToken).getBody();
        } catch (SecurityException e) {
            log.info("Invalid JWT signature.");
        } catch (MalformedJwtException e) {
            log.info("Invalid JWT token.");
        } catch (ExpiredJwtException e) {
            log.info("Expired JWT token.");
        } catch (UnsupportedJwtException e) {
            log.info("Unsupported JWT token.");
        } catch (IllegalArgumentException e) {
            log.info("JWT token compact of handler are invalid.");
        }

        return null;
    }
}
