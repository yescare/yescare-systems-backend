package com.yescare.springconfig;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class TokenValidFaildException extends RuntimeException {

    public TokenValidFaildException() {
        //super(ErrorCode.TOKEN_GENERATION_FAILED.getMessage());
        log.info("ERROR TOKEN GENERATION FAILED");
    }
}
