package com.yescare.springconfig;

import org.springframework.security.core.Authentication;

import java.util.Date;

public interface AuthTokenProvider<T> {
    T createAuthToken(String id, String role, Date expireDate);
    T convertAuthToken(String token);
    Authentication getAuthentication(T authToken);
}
