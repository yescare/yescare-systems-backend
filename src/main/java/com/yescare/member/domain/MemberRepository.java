package com.yescare.member.domain;

import com.yescare.common.jpaspec.Specification;
import com.yescare.member.domain.Member;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface MemberRepository {

    Optional<Member> selectByLoginId(String loginId);
}
