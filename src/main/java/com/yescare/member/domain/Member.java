package com.yescare.member.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.bind.Name;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
public class Member {

    @Id
    @JsonIgnore
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private String id;

    private String name;

    @Column(length = 16, nullable = true, unique = true)
    private String loginId;

    @Column(length = 30, nullable = true)
    private String password;

    @Column(name = "auth_code")
    @Enumerated(EnumType.STRING)
    private Role role;

    public Member(String id, String loginId, String name, Role role) {
        this.id = id;
        this.loginId = loginId;
        this.name = name;
        this.role = role;
    }

}
