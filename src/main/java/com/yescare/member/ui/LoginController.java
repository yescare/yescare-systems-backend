package com.yescare.member.ui;

import com.yescare.member.domain.Member;
import com.yescare.springconfig.CommonResponse;
import com.yescare.springconfig.JwtAuthToken;
import com.yescare.springconfig.JwtAuthTokenProvider;
import com.yescare.springconfig.service.LoginService;
import com.yescare.springconfig.exception.LoginFailedException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Collections;
import java.util.Map;
import java.util.Optional;

@RestController
@RequiredArgsConstructor
@Slf4j
public class LoginController {

    private final LoginService loginService;

    private final JwtAuthTokenProvider jwtAuthTokenProvider;


    /**
     * Login 로그인
     * @param loginUser
     * @return CommonResponse
     */
    @PostMapping("/login")
    public CommonResponse login(@RequestBody Member loginUser) {

        Optional<Member> hasMember = loginService.login(loginUser.getLoginId(), loginUser.getPassword());

        if (hasMember.isPresent()) {
            String jwtAuthToken = jwtAuthTokenProvider.createToken(hasMember.get());

            return CommonResponse.builder()
                    .code("LOGIN_SUCCESS")
                    .status(200)
                    .message(jwtAuthToken)
                    .build();
        } else {
            throw new LoginFailedException();
        }
    }

    /**
     * Logout 로그아웃
     * @param request
     * @param response
     * @return
     */
    @GetMapping("/loggedOut")
    public CommonResponse logout(HttpServletRequest request, HttpServletResponse response) {

        String token = jwtAuthTokenProvider.resolveToken(request);
        String invalidateToken = jwtAuthTokenProvider.invalidateToken(token);

        request.getSession().invalidate(); // session invalidate
        response.setHeader("X-AUTH-TOKEN", invalidateToken);

        return CommonResponse.builder()
                .code("LOGOUT_SUCCESS")
                .status(200)
                .message("로그아웃에 성공하였습니다.")
                .build();
    }

    /**
     * 테스트 코드
     */
    @GetMapping("/user/test")
    public void testUserApi() {

        // security context 확인
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        UserDetails userInfo = (UserDetails) auth.getPrincipal();

        log.debug("this is test.");
    }
}
