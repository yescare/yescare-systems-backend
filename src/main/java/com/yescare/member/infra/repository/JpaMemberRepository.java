package com.yescare.member.infra.repository;

import com.yescare.common.jpa.JpaQueryUtils;
import com.yescare.common.jpaspec.Specification;
import com.yescare.member.domain.Member;
import com.yescare.member.domain.MemberRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.swing.*;
import java.util.List;
import java.util.Optional;

@Repository
public class JpaMemberRepository implements MemberRepository {

    @PersistenceContext
    private EntityManager em;

    @Override
    public Optional<Member> selectByLoginId(String loginId) {

        String selectQuery =
                "select m " +
                "from Member m " +
                "where m.loginId = :loginId ";

        TypedQuery<Member> query =
                em.createQuery(selectQuery, Member.class);

        query.setParameter("loginId", loginId);
        Optional<Member> member = query.getResultList().stream().findFirst();

        return member;
    }

    public String save(Member member) {
        em.persist(member);
        return member.getId();
    }
}

