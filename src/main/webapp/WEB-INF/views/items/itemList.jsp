<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Insert title here</title>
		<jsp:include page="../header.jsp"></jsp:include>
	</head>
	<script>

		//전역변수 
		var prodId = 0;
		var prodName = "";
		var itemId = 0;
		var unitYn = "";
		var unitCnt = 0;
		var userMaxCnt = 20;
		var itemList = [];
		
		$(document).ready(function () {
	
			//1. 제품선택 
			$("#prodList li").on("click", function(){
				prodId = $(this).attr('id'); //선택한 제품의 id 넣기
				prodName = $(this).text(); //제품 이름 넣기 
				unitYn = $(this).data('unityn'); //유저수사용여부
				console.log("prodId=" + prodId);
				console.log("prodName=" + prodName);
				console.log("unitYn=" + unitYn);
			});
			
		});
	
	
		//******************************************************
		// 2. 제품선택 후 제품유형 아이템 가져오는 함수 
		//******************************************************
		function fncGetItems() {
	
			if (prodId == 0) {
				alert("제품을 선택해 주세요");
				return false;
			}
			
			$.ajax({
				type : "GET",
				contentType : "text",
				url : "/items/" + prodId + "/list",
				datatype : 'json',
				success : function(res) {

					itemList = res;
					
					$("#itemList").empty();
					$("#buildList").empty();
					
					res.map(item => {
						console.log(item);
						
						var html = "";
					 	html += "<li id='" + item.id + "' data-name='"+ item.name +"'>";
						html += 	item.name + "   : " + item.price;		 	
					 	html += "</li>";

					 	if (unitYn == "Y") {
							$('#itemList').append(html);
					 	} else {
							$('#buildList').append(html);
					 	}

						//제품 아이템 유형 선택 
						$("#itemList li").on("click", function(){
							itemId = $(this).attr('id'); //선택한 제품의 id 넣기
							console.log("itemId=" + itemId);
						});
	
						//구축 유형 선택 
						$("#buildList li").on("click", function(){
							itemId = $(this).attr('id'); //선택한 제품의 id 넣기
							itemName = $(this).data('name'); //선택한 제품의 id 넣기
							console.log("itemId=" + itemId);
							console.log("buildType = itemName=" + itemName);
							$("#buildType").text(itemName);
						});
	
					});
	
					console.log(itemList);
				},
				error : function(error) {
					alert("오류 발생" + error);
				}
			});

			if (unitYn == "Y") {
				$("#stepItem").css("display", "block");
			} else {
				$("#stepBuild").css("display", "block");
			}
		}
	
		//******************************************************
		// 3. 사융유저수 입력창 보이기 
		//******************************************************
		function fncViewSetUserCount() {

			//TODO prod에 따라서 보여주는 다음 단계가 달라져야 한다. 
			//prodId = 
			$("#stepUser").css("display", "block");
			
			var itemDetail = itemList.filter(item => {
				if (item.id == itemId) {
					return item;
				}
			});
			
			console.log("itemDetail=" + itemDetail[0].name);
			console.log("userMaxCount=" + itemDetail[0].name);
			//$("#itemDescrition").val('');
			
		}
	
		//******************************************************
		// 4. 지원금 계산 
		//******************************************************
		function fncViewCalculate() {
			$("#stepCalc").css("display", "block");

			if (unitYn == "Y") {
				unitCnt = Number($("#txt_UserCount").val());
				console.log(Number(unitCnt) > 0, unitCnt, userMaxCnt, typeof(unitCnt));
				
				//유저수 체크 
				if (!(unitCnt > 0 && unitCnt <= userMaxCnt)) {
					alert("사용자수가 유효하지 않습니다.");
					return false;
				}

				$("#UNIT_CNT").val(unitCnt); //유저수
				$("#unitCnt").text(unitCnt); //유저수
				$("#li_unitCnt").css("display", "block");
			} else {
				$("#li_buildType").css("display", "block");
			}
			
			var itemDetail = itemList.filter(item => {
				if (item.id == itemId) {
					return item;
				}
			});
	
			var optTotalPrice = 0;
			$("#priceList").empty();
			
			if (itemDetail.length > 0 && itemDetail[0].itemOptions.length > 0) {
				itemDetail[0].itemOptions.map(opt => {

					var html = "";
					var price = opt.price;

		 			html += "<li id='" + opt.id + "'>";

		 			if (opt.unitCalYn == "Y") {
						price = price * unitCnt;
				 	} 
				 	
					html += 	opt.optName + "   : " + price;	 	
				 	html += "</li>";
				 	
				 	optTotalPrice += price;
				 	
					$('#priceList').append(html);
				});
				
			}
			
			var item = itemDetail[0];
			var totalprice = (item.price == 0 ? optTotalPrice : item.price);
			var selfPrice = totalprice - (item.govPrice + item.douzonePrice);
			console.log(item);
			console.log(totalprice, selfPrice);

			//할인율 = 1-(할인액÷(단가×수량))
			var discountRate = Math.floor(((totalprice - selfPrice) / totalprice) * 100); 
			console.log("discountRate=" + discountRate);
			
			//하위 붙이기
			var html = "";
			html += "	<li id='" + item.id + "'>";
			html += " 		총비용  : " + totalprice;		 	
		 	html += "	</li>";
			html += "	<li>정부지원금 : "+item.govPrice+"</li>";
			html += "	<li>더존지원금 : "+item.douzonePrice+"</li>";
			html += "	<li>자부담금 	: "+selfPrice+"</li>";
			html += "	<li>정부지원금 적용으로 총 "+discountRate+"% 할인 혜택을 받으셧습니다.</li>";

			$('#priceList').append(html);

			//form 넘겨줄 계산값넣기
			$("#PRICE").val(totalprice); //총금액
			$("#GOV_PRICE").val(item.govPrice); //정부지원금
			$("#DOUZONE_PRICE").val(item.douzonePrice); //더존지원금
			$("#COMP_SELF_PRICE").val(selfPrice); //자부담금
			$("#DISCOUNT_RETE").val(discountRate); //할인율

			//viewForm 보여줄값 다시넣기
			$("#prodName").text(prodName); //제품명
			$("#compPrice").text(selfPrice); //자부담금
		}
	
		//5. 유저 정보입력 폼 
		function fncViewInForm() {
			$("#stepInForm").css("display", "block");
		}
	
		//6. 작성한 폼 확인하는 
		function fncViewConfirmForm() {
			$("#stepConfirmForm").css("display", "block");
			
			var chkAgree = $("input:checkbox[name='chkAgree']:checked").val();
			
			if (chkAgree != "Y") {
				alert("이용동의에 동의하셔야 합니다.");
				return false;
			}

			$("#compName").text($("#COMP_NAME").val());
			$("#crNum").text($("#CR_NUM").val());
			$("#userName").text($("#USER_NAME").val());
			$("#mobileNum").text($("#MOBILE_NUM").val());
			$("#emailAddr").text($("#EMAIL_ADDR").val());
		}
	
		// 7. 제출
		function fncSubmit() {
			
			var chkAgree = $("input:checkbox[name='chkAgree']:checked").val();
			
			if (chkAgree != "Y") {
				alert("이용동의에 동의하셔야 합니다.");
				return false;
			}
	
			var formData = $("form[name=inform]").serialize();
			console.log(formData);
	
			$.ajax({
				type : "POST",
				contentType : "application/x-www-form-urlencoded ; charset=utf-8",
				url : "/items/" + itemId + "/inform/submit",
				data : formData,
				datatype : 'json',
				success : function(res) {
					console.log(res);
	
					if (res == "ok") {
						alert("신청이 완료되었습니다.");
						window.location.href = "/";
					} else {
						alert("신청에 실패하였습니다.");
					}
				},
				error : function(error) {
					alert("오류 발생" + error);
				}
			});
			
		}
	</script>
	<jsp:include page="../header.jsp" />
	<body>
		<div id="wrap" class="login_wrap">
			<h2>Hello!</h2>
			<div id="stepProd">
				<h2>STEP 1. 제품 선택</h2>
				<ul id="prodList">
					<c:forEach items="${items}" var="item">
						<li id="${item.id}" data-unityn="${item.unitYn}">${item.name}</li>
					</c:forEach>
				</ul>
				<button onclick="fncGetItems();">다음단계</button>
			</div>
			
			<div id="stepItem" style="display:none;">
				<h2>STEP 2. 제품 유형</h2>
				<ul id="itemList"></ul>
				<button onclick="fncViewSetUserCount();">다음단계</button>
			</div>
			
			<div id="stepBuild" style="display:none;">
				<h2>STEP 2. 구축 형태</h2>
				<ul id="buildList"></ul>
				<button onclick="fncViewCalculate();">다음단계</button>
			</div>
			
			<div id="stepUser" style="display:none;">
				<h2>STEP 3. 사용유저 - 유저수를 입력해 주시기 바랍니다.</h2>
				<input id="txt_UserCount" type="text">
				<span id="itemDescrition"></span>
				<button onclick="fncViewCalculate();">다음단계</button>
			</div>
			
			<div id="stepCalc" style="display:none;">
				<h2>지원금확인</h2>
				<ul id="priceList"></ul>
				<button onclick="fncViewInForm();">다음단계</button>
			</div>
			
			<div id="stepInForm" style="display:none;">
				<form name="inform">
					<h2>상담신청</h2>
					<input type="text" id="COMP_NAME" name="compName" value="" placeholder="회사명을 입력해 주세요." ><br/>
					<input type="text" id="CR_NUM" name="crNum" value="" placeholder="사업자번호를 입력해 주세요." ><br/>
					<input type="text" id="USER_NAME" name="userName" value="" placeholder="이름을 입력해 주세요." ><br/>
					<input type="text" id="MOBILE_NUM" name="mobileNum" value="" placeholder="휴대전화번호을 입력해 주세요." ><br/>
					<input type="text" id="EMAIL_ADDR" name="emailAddr" value="" placeholder="이메일을 입력해 주세요." ><br/>
					
					<textarea rows="" cols="">수집및이용동의</textarea><br/>
					<input type="checkbox" name="chkAgree" value="Y">이용동의합니다.<br/>
		
					<input type="hidden" id="COMP_SELF_PRICE" name="compSelfPrice" value="">
					<input type="hidden" id="DOUZONE_PRICE" name="douzonePrice" value="">
					<input type="hidden" id="GOV_PRICE" name="govPrice" value="">
					<input type="hidden" id="PRICE" name="price" value="">
					<input type="hidden" id="UNIT_CNT" name="unitCnt" value="0">
					<input type="hidden" id="DISCOUNT_RETE" name="discountRate" value="">

				</form>
				<button onclick="fncViewConfirmForm();">다음단계</button>
			</div>
			
			<div id="stepConfirmForm" style="display:none;">
				<h2>최종확인</h2>
				<h4>제품정보</h4>
				<ul>
					<li>신청제품 : <span id="prodName"></span></li>
					<li id="li_unitCnt" style="display: none;">신청유저 : <span id="unitCnt"></span></li>
					<li id="li_buildType" style="display:none;">도입유형 : <span id="buildType"></span></li>
					<li>기업부담금 : <span id="compPrice"></span>원</li>
				</ul>
				<h4>고객정보</h4>
				<ul>
					<li>회사명 : <span id="compName"></span></li>
					<li>이름 : <span id="userName"></span></li>
					<li>휴대전화번호 : <span id="mobileNum"></span></li>
					<li>이메일 : <span id="emailAddr"></span></li>
				</ul>
				<button onclick="fncSubmit();">확인</button>
			</div>
			
		</div>
	</body>
</html>