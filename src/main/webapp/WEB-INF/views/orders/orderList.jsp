<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<jsp:include page="../header.jsp"></jsp:include>
<script type="text/javascript">


	$(document).ready(function () {
	
		fncGetFindOrders();
		
	});

	
	//******************************************************
	// 견적 리스트 조회 
	//******************************************************
	function fncGetFindOrders() {

		$("input[name=compName]").val($("#txt_CompName").val());
		$("input[name=prodName]").val($("#txt_ProdName").val());
				
		//조회값셋팅
		var formData = $("form[name=searchform]").serialize();
		console.log(formData);
		
		$.ajax({
			type : "POST",
			contentType : "application/x-www-form-urlencoded ; charset=utf-8",
			url : "/orders/list",
			data : formData,
			datatype : 'json',
			success : function(res) {
				console.log(res);
			},
			error : function(error) {
				alert("오류 발생" + error);
			}
		});
	
	}

</script>
<body>
<form name="searchform">
	<input type="hidden" value="" name="compName">
	<input type="hidden" value="" name="createStartDt">
	<input type="hidden" value="" name="createEndDt">
	<input type="hidden" value="" name="prodName">
</form>
	<h2>Hello!</h2>
	<div>견적주문 조회 페이지</div>
	회사이름:<input type="text" id="txt_CompName">
	시작일:<input type="hidden" id="">
	종료일:<input type="hidden" id="">
	제품명:<input type="text" id="txt_ProdName">
	
	<button onclick="fncGetFindOrders();">검색</button>
	
	<div class="noticeTable mb10">
		<div class="headerTitle">
			<ul>
				<li class="">날자</li>
				<li class="">제품명</li>
				<li class="">고객명</li>
				<li class="">담당자명</li>
				<li class="">연락처</li>
				<li class="">구축형태</li>
				<li class="">유저수</li>
				<li class="">총금액</li>
				<li class="">정부지원금</li>
				<li class="">더존지원금</li>
				<li class="">기업부담금</li>
				<li class="">할인율</li>
				<li class="">상담현황</li>
				<li class="">도입여부</li>
			</ul>
		</div>

		<ol id="ol_itemList">
			<!-- 반복 -->
		</ol>
	</div>
	
	<!-- 페이징 -->
	<div class="paging" id="divPager"></div>
</body>
</html>