<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Insert title here</title>
</head>
<jsp:include page="../header.jsp"></jsp:include>
<body>
<body>
<h2>Hello!</h2>
<div>카테고리 선택 페이지 </div>
<c:forEach items="${prods}" var="item">
    <tr>
        <td>${item.id}</td>
        <td>${item.name}</td>
        <a href="/items/${item.id}/list">신청하기</a>
    </tr>
</c:forEach>
</body>

</body>
</html>